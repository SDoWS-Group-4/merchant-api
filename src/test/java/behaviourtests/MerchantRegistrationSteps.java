package behaviourtests;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import messaging.Event;
import messaging.MessageQueue;
import dtu.Merchant;
import dtu.MerchantRegistrationService;

public class MerchantRegistrationSteps {

	// private CompletableFuture<Event> publishedEvent = new CompletableFuture<>();
	
	// private MessageQueue q = new MessageQueue() {

	// 	@Override
	// 	public void publish(Event event) {
	// 		publishedEvent.complete(event);
	// 	}

	// 	@Override
	// 	public void addHandler(String eventType, Consumer<Event> handler) {
	// 	}
		
	// };
	// private MerchantRegistrationService service = new MerchantRegistrationService(q);
	// private CompletableFuture<Merchant> registeredMerchant = new CompletableFuture<>();
	// private Merchant merchant;

	// public MerchantRegistrationSteps() {
	// }

	// @Given("there is a merchant with empty id")
	// public void thereIsAMerchantWithEmptyId() {
	// 	merchant = new Merchant();
	// 	merchant.setName("James");
	// 	assertNull(merchant.getID());
	// }

	// @When("the merchant is being registered")
	// public void theMerchantIsBeingRegistered() {
	// 	// We have to run the registration in a thread, because
	// 	// the register method will only finish after the next @When
	// 	// step is executed.
	// 	// step is executed.
	// 	new Thread(() -> {
	// 		var result = service.register(merchant);
	// 		registeredMerchant.complete(result);
	// 	}).start();
	// }

	// @Then("the {string} event is sent")
	// public void theEventIsSent(String string) {
	// 	Event event = new Event(string, new Object[] { merchant });
	// 	//assertEquals(event,publishedEvent.join());
	// }

	// @When("the {string} event is sent with non-empty id")
	// public void theEventIsSentWithNonEmptyId(String string) {
	// 	// This step simulate the event created by a downstream service.
	// 	var c = new Merchant();
	// 	c.setName(merchant.getName());
	// 	c.setID("123");
	// 	service.handleMerchantIdAssigned(new Event("..",new Object[] {c}));
	// }

	// @Then("the merchant is registered and his id is set")
	// public void theMerchantIsRegisteredAndHisIdIsSet() {
	// 	// Our logic is very simple at the moment; we don't
	// 	// remember that the merchant is registered.
	// 	assertNotNull(registeredMerchant.join().getID());
	// }
}
