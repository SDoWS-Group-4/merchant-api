package dtu;

import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

import messaging.Event;
import messaging.MessageQueue;

public class MerchantRegistrationService {

	private MessageQueue queue;
	private Map<CorrelationId, CompletableFuture<String>> correlations = new ConcurrentHashMap<>();

	public MerchantRegistrationService(MessageQueue q) {
		queue = q;
		queue.addHandler("MerchantIdAssigned", this::handleMerchantIdAssigned);
		queue.addHandler("MerchantNotAssigned", this::handleMerchnatNotAssigned);

		queue.addHandler("MerchantDeregistered", this::handleMerchantDeregistered);
		queue.addHandler("MerchantNotDeregistered", this::handleMerchantNotDeregistered);
	}

	public String register(Merchant m) throws Exception {
		try {
			var correlationId = CorrelationId.randomId();
			correlations.put(correlationId, new CompletableFuture<>());
			Event event = new Event("MerchantRegistrationRequested", new Object[] { m, correlationId });
			queue.publish(event);

			return correlations.get(correlationId).join();
		} catch (Exception e) {
			e.printStackTrace();
			if (e.getCause() != null) {
				if (e.getCause().getClass() == IllegalArgumentException.class) {
					throw new IllegalArgumentException(e.getCause().getMessage());
				}
			}
			throw new Exception(e.getMessage());
		}
	}

	public String deregister(String id) throws Exception {
		try {
			var correlationId = CorrelationId.randomId();
			correlations.put(correlationId, new CompletableFuture<>());
			Event event = new Event("MerchantDeregistrationRequested", new Object[] { id, correlationId });
			queue.publish(event);

			return correlations.get(correlationId).join();
		} catch (Exception e) {
			e.printStackTrace();
			if (e.getCause() != null) {
				if (e.getCause().getClass() == IllegalArgumentException.class) {
					throw new IllegalArgumentException(e.getCause().getMessage());
				}
				if (e.getCause().getClass() == NoSuchElementException.class) {
					throw new IllegalArgumentException(e.getCause().getMessage());
				}
			}
			throw new Exception(e.getMessage());
		}
	}

	public void handleMerchantDeregistered(Event e) {
		var s = e.getArgument(0, String.class);
		var c = e.getArgument(1, CorrelationId.class);

		correlations.get(c).complete(s);
	}

	public void handleMerchantNotDeregistered(Event e) {
		var exception = e.getArgument(0, NoSuchElementException.class);
		var mid = e.getArgument(1, CorrelationId.class);

		// correlations.get(cid).complete(exception);
		correlations.get(mid).completeExceptionally(exception);
	}

	public void handleMerchantIdAssigned(Event e) {
		var merchantId = e.getArgument(0, String.class);
		var c = e.getArgument(1, CorrelationId.class);

		correlations.get(c).complete(merchantId);
	}

	public void handleMerchnatNotAssigned(Event e) {
		var exception = e.getArgument(0, IllegalArgumentException.class);
		var mid = e.getArgument(1, CorrelationId.class);

		// correlations.get(cid).complete(exception);
		correlations.get(mid).completeExceptionally(exception);
	}
}
