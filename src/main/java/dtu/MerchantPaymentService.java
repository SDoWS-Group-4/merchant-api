package dtu;

import messaging.Event;
import messaging.MessageQueue;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

public class MerchantPaymentService {

	private MessageQueue queue;
	private Map<CorrelationId, CompletableFuture<String>> correlations = new ConcurrentHashMap<>();

	public MerchantPaymentService(MessageQueue q) {
		queue = q;
		queue.addHandler("PaymentSuccessful", this::handlePaymentSuccessful);
		queue.addHandler("PaymentNotSuccessful", this::handlePaymentNotSuccessful);

	}

	public String pay(Payment payment) throws Exception {
		try {
			var correlationId = CorrelationId.randomId();
			correlations.put(correlationId, new CompletableFuture<>());
			Event event = new Event("PaymentRequested", new Object[] { payment, correlationId });
			queue.publish(event);

			return correlations.get(correlationId).join();
		} catch (Exception e) {
			if (e.getCause() != null) {
				throw new Exception(e.getCause().getMessage());
			} else {
				throw new Exception(e.getMessage());
			}
		}
	}


	public void handlePaymentSuccessful(Event e) {
		var s = e.getArgument(0, String.class);
		var c = e.getArgument(1, CorrelationId.class);

		correlations.get(c).complete(s);
	}

	public void handlePaymentNotSuccessful(Event e) {
		//var exception = e.getArgument(0, IllegalArgumentException.class);
		var exception = e.getArgument(0, Exception.class);
		var mid = e.getArgument(1, CorrelationId.class);

//		correlations.get(mid).complete(exception);
		correlations.get(mid).completeExceptionally(exception);
//		correlations.get(mid).completeExceptionally(exception);
	}
}
