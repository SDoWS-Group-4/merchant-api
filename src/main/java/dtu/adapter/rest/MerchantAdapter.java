package dtu.adapter.rest;

import java.util.ArrayList;
import java.util.List;

import dtu.*;

public class MerchantAdapter {
    MerchantRegistrationService service = new MerchantRegistrationFactory().getService();
    MerchantPaymentService payService = new MerchantPaymentFactory().getService();
    PaymentService paymentService = new PaymentServiceFactory().getService();

    public String pay(Payment payment) throws Exception {

        // Rabbit mq with payment
        return payService.pay(payment);
    }

    public List<Payment> getReport(String id) throws Exception {
        if (id.isEmpty() || id == null) {throw new IllegalArgumentException("Missing fields");}
        return paymentService.getReport(id);
    }

    public String register(Merchant merchant) throws Exception {
        if (isNullOrEmpty(merchant)){throw new IllegalArgumentException("Missing fields");}

            return service.register(merchant);
   
    }

    public String deregister(String id) throws Exception {
        
            return service.deregister(id);
    }

    private Boolean isNullOrEmpty(Merchant merchant){
        return merchant.getBankAccount() == null ||
        merchant.getCprNumber() == null ||
        merchant.getFirstName() == null ||
        merchant.getLastName() == null ||
        merchant.getBankAccount().isBlank() ||
        merchant.getCprNumber().isBlank() ||
        merchant.getFirstName().isBlank() ||
        merchant.getLastName().isBlank();
    }
}
