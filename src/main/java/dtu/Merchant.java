package dtu;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

public class Merchant implements Serializable {

    @Getter @Setter private String id;
    @Getter @Setter private String firstName;
    @Getter @Setter private String lastName;
    @Getter @Setter private String cprNumber;
    @Getter @Setter private String bankAccount;

    public Merchant() {
    }

    public Merchant(String id, String firstName, String lastName, String cprNumber, String bankAccount) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.cprNumber = cprNumber;
        this.bankAccount = bankAccount;
    }
}
