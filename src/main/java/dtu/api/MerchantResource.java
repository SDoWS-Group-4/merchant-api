package dtu.api;


import java.util.List;
import java.util.NoSuchElementException;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import dtu.Exceptions.MyBadRequestException;
import dtu.Exceptions.MyNotFoundException;
import dtu.Merchant;
import dtu.Payment;
import dtu.adapter.rest.MerchantAdapter;

@Path("/merchant")
public class MerchantResource {

    MerchantAdapter merchAdapter = new MerchantAdapter();

    @Path("/register")
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public String registerMerchant(Merchant merchant) throws Exception {
        try {
            return merchAdapter.register(merchant);
        } catch (Exception e) {
            throw new MyBadRequestException(e.getMessage());
        }
	}

    @Path("/deregister")
    @DELETE
    @Consumes("application/json")
    @Produces("application/json")
    public String deregisterMerchant(@QueryParam("id") String id) throws Exception {
        try {
            return merchAdapter.deregister(id);
        } catch (IllegalArgumentException e) {
            throw new MyBadRequestException(e.getMessage());
        } catch (NoSuchElementException ex) {
            throw new MyNotFoundException(ex.getMessage());
        } catch (Exception ex1) {
            throw new Exception(ex1.getMessage());
        }
    }

	@Path("/pay")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String payAPI(Payment payment) throws Exception {
        try {
            return merchAdapter.pay(payment); // Adapter is resposible for logic
        } catch (Exception e) {
            throw new MyBadRequestException(e.getMessage());
        }
    }

    @Path("/report")
    @GET
    @Produces("application/json")
    public List<Payment> getReportAPI(@QueryParam("id") String id) throws MyBadRequestException, MyNotFoundException {
        try {
            return merchAdapter.getReport(id);
        } catch (NoSuchElementException ex) {
            throw new MyNotFoundException(ex.getMessage());
        } catch (Exception ex1) {
            throw new MyBadRequestException(ex1.getMessage());
        }

    }
}
