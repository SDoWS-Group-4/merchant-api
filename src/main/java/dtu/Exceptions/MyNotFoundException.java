package dtu.Exceptions;
 
public class MyNotFoundException extends Exception {
    private static final long serialVersionUID = 1L;
 
    public MyNotFoundException() {
        super();
    }
    public MyNotFoundException(String msg)   {
        super(msg);
    }
    public MyNotFoundException(String msg, Exception e)  {
        super(msg, e);
    }
}